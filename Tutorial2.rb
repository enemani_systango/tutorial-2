require 'csv'

INPUT_FILENAME = 'input.csv'
OUTPUT_FILENAME = 'output.csv'
IND = "India"
US = "US"
UK = "UK"

class Item
	def initialize(id, price, country)
		@id, @price, @country = id, price, country
	end
	@sales_tax = 0
	
	def set_sales_tax
		@sales_tax = calculate_sales_tax
	end


	def calculate_sales_tax
		@country = @country.to_s
		return sales_tax_india if IND == @country
		return sales_tax_us if US == @country
		return sales_tax_uk if UK == @country
		return  'N.A'
	end

	def sales_tax_india
		return 0 if @price <= 100
		return (@price-100) * 0.05 if @price > 100 && @price <= 500
		return 20+(@price-500)*0.2
	end

	def sales_tax_us
		return Math.sqrt(@price)
	end

	def sales_tax_uk
		(@price)*0.03
	end

	def get_id
		@id
	end
	def get_price
		@price
	end
	def get_country
		@country
	end
	def get_sales_tax
		@sales_tax
	end
		
end

class InputReader
	def initialize(filename)
		@filename = filename
	end

	def validate_file
		(File.file?(@filename))? "Input file exists" : "Input file doesn't exist"
		puts "Input File Opened "
	end

	def read_file
		return CSV.read(@filename, converters: :numeric, headers:true)
	end
end

class ItemInitializer
	def initialize(input_reader,item_factory)
		@input_reader, @item_factory = input_reader, item_factory
	end	

	def assign_row_to_item()
		input_array = @input_reader.read_file
		input_array.each do |row|
			item_from_row = Item.new(row_id(row),row_price(row),row_country(row))
			@item_factory.add(item_from_row)
		end
	end
	def row_id(row)
		row[0].to_i 
	end
	def row_price(row)
		row[1].to_i 
	end
	def row_country(row)
		row[2].to_s 
	end

end

class ItemFactory
	def item_array
		@item_array ||= []
	end
		
	def add(item)
		item_array << item
	end

	def get_item_array
		item_array
	end

end

class ItemUpdater
	def initialize(item_factory)
		@item_factory = item_factory
	end

	def update_item_factory
		@item_factory.get_item_array.each do |item|
			item.set_sales_tax
		end
	end
end

class OutputWriter
	def initialize(item_factory, output_filename)
		@item_factory, @output_filename = item_factory, output_filename
	end

	def write_to_output_file
		CSV.open(@output_filename,'w') do |csv_object|
			@item_factory.get_item_array.each do |item|
				csv_object << [item.get_id,item.get_price,item.get_country,item.get_sales_tax,]
			end
		end
	end

	def validate_output_file
		return "Error! Output file is not created" if !(File.file?(@output_filename))
		puts "Output File Created "
	end
end

class GetMySalesTax
	def initialize(input_file, output_file)
		@input_file, @output_file = input_file, output_file
	end

	def run
		input_reader_instance = InputReader.new(@input_file)
		item_factory_instance = ItemFactory.new
		item_initializer_instance = ItemInitializer.new(input_reader_instance,item_factory_instance) 
		item_updater_instance = ItemUpdater.new(item_factory_instance)
		output_writer_instance = OutputWriter.new(item_factory_instance,@output_file)

		input_reader_instance.validate_file
		item_initializer_instance.assign_row_to_item
		item_updater_instance.update_item_factory
		output_writer_instance.write_to_output_file
		output_writer_instance.validate_output_file
	end
end

sales_tax_calculator = GetMySalesTax.new(INPUT_FILENAME, OUTPUT_FILENAME)
sales_tax_calculator.run
